//
//  ApiConstants.swift
//  Evaluation
//
//  Created by Paolo Eduardo Arámbulo Calderón on 9/9/20.
//  Copyright © 2020 Paolo Eduardo Arámbulo Calderón. All rights reserved.
//

import Foundation

struct ApiConstants {
    struct security {
        static let login = "http://api-movies.pappstest.com/api/v1/auth/login"
    }
    
    struct movie {
        static let paginate = "http://api-movies.pappstest.com/api/v1/movies"
    }
}
