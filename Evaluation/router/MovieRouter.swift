//
//  MovieRouter.swift
//  Evaluation
//
//  Created by Paolo Eduardo Arámbulo Calderón on 9/10/20.
//  Copyright © 2020 Paolo Eduardo Arámbulo Calderón. All rights reserved.
//

import Foundation
import UIKit

class MovieRouter: BaseRouter {
    func goToDetail(poMovieDetail: MovieDetail){
        let loStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let loViewController = loStoryboard.instantiateViewController(withIdentifier:
            "idMovieDetailViewController") as! MovieDetailViewController
        loViewController.goMovieDetail = poMovieDetail
        goViewController.show(loViewController, sender: nil)
    }
}
