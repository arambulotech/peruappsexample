//
//  MovieService.swift
//  Evaluation
//
//  Created by Paolo Eduardo Arámbulo Calderón on 9/10/20.
//  Copyright © 2020 Paolo Eduardo Arámbulo Calderón. All rights reserved.
//

import Foundation
import RxSwift

class MovieService: MovieUseCase {
    
    func paginate(psToken: String) -> Observable<Response<[Movie]?>> {
        return RestBase.request(poRequest: nil, psUrl: ApiConstants.movie.paginate, poMethod: .GET, psToken: psToken)
    }
    
}
