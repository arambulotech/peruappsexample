//
//  StringExtension.swift
//  Evaluation
//
//  Created by Paolo Eduardo Arámbulo Calderón on 9/10/20.
//  Copyright © 2020 Paolo Eduardo Arámbulo Calderón. All rights reserved.
//

import Foundation

extension String {
    
    ///REMOVE SPACES OR LINE JUMPS AT THE BEGINNING OR END OF A STRING
    ///QUITA LOS ESPACIOS O SALTOS DE LINEA AL PRINCIPIO O FINAL DE UN STRING
    public func trim() -> String {
        return trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
}
