//
//  Response.swift
//  Evaluation
//
//  Created by Paolo Eduardo Arámbulo Calderón on 9/9/20.
//  Copyright © 2020 Paolo Eduardo Arámbulo Calderón. All rights reserved.
//

import Foundation

struct Response<M: Codable>: Codable {
    var status: Int? = nil
    var message: String? = ""
    var data: M? = nil
}
