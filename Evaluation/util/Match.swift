//
//  Match.swift
//  Evaluation
//
//  Created by Paolo Eduardo Arámbulo Calderón on 9/10/20.
//  Copyright © 2020 Paolo Eduardo Arámbulo Calderón. All rights reserved.
//

import Foundation

struct Match {
    
    static func validate(psText: String, psRegex: String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: psRegex, options: .caseInsensitive)
            return regex.firstMatch(in: psText, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, psText.count)) != nil
        } catch {
            return false
        }
    }
    
}

enum Regex: String {
    case MAIL = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$"
}
