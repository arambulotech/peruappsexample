//
//  AuthUseCase.swift
//  Evaluation
//
//  Created by Paolo Eduardo Arámbulo Calderón on 9/9/20.
//  Copyright © 2020 Paolo Eduardo Arámbulo Calderón. All rights reserved.
//

import Foundation
import RxSwift

protocol AuthUseCase {
    
    func login(poLoginRequest: LoginRequest) -> Observable<Response<Token?>>
    
}
