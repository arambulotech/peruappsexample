//
//  LoginViewController.swift
//  Evaluation
//
//  Created by Paolo Eduardo Arámbulo Calderón on 9/9/20.
//  Copyright © 2020 Paolo Eduardo Arámbulo Calderón. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    //MARK: OUTLETS
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    
    //MARK: GLOBAL VARIABLES
    private let goPreferences = UserDefaults.standard
    private var goLoginRouter: LoginRouter!
    private var goLoginPresenter: LoginPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initRouter()
        initPresenter()
    }
    
    func initRouter() {
        goLoginRouter = LoginRouter(poViewController: self)
    }
    
    func initPresenter() {
        goLoginPresenter = LoginPresenter(poAuthService: AuthService())
        goLoginPresenter.goView = self
    }
    
    func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    func setToken(psToken: String) {
        goPreferences.set(psToken, forKey: "KEY_TOKEN")
        goPreferences.synchronize()
    }
    
    func validateEmail(psEmail: String, psRegex: String) -> Bool {
        return Match.validate(psText: psEmail, psRegex: psRegex)
    }
    
    func validateField(psEmail: String, psPassword: String) {
        
    }
    
    @IBAction func login(_ sender: Any) {
        hideKeyboard()
        
        guard txtUsername.text?.trim() != "" && txtPassword.text?.trim() != "" else {
            return AlertViewUtil.showAccept(poViewController: self, psMessage: "Fill empty fields")
        }
        
        if (validateEmail(psEmail: txtUsername.text!, psRegex: Regex.MAIL.rawValue)) {
            goLoginPresenter.login(psMail: txtUsername.text ?? "", psPassword: txtPassword.text ?? "")
        } else {
            AlertViewUtil.showAccept(poViewController: self, psMessage: "Enter valid email")
        }
    }
    
}

extension LoginViewController: LoginView {
    
    func onSuccessLogin(psToken: String, psMail: String, psPassword: String) {
        setToken(psToken: psToken)
        
        DispatchQueue.main.async {
            self.goLoginRouter.goToMain(psToken: psToken, psMail: psMail, psPassword: psPassword)
        }
    }
    
    func onError(psMessage: String) {
        DispatchQueue.main.async {
            AlertViewUtil.showAccept(poViewController: self, psMessage: psMessage)
        }
    }
    
}
