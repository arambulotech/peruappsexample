//
//  MovieDetailViewController.swift
//  Evaluation
//
//  Created by Paolo Eduardo Arámbulo Calderón on 9/10/20.
//  Copyright © 2020 Paolo Eduardo Arámbulo Calderón. All rights reserved.
//

import UIKit
import Kingfisher

class MovieDetailViewController: UIViewController {
    
    //MARK: OUTLETS
    @IBOutlet weak var imgMovie: UIImageView!
    @IBOutlet weak var lblMovieTitle: UILabel!
    @IBOutlet weak var lblMovieDescription: UILabel!
    
    //MARK: GLOBAL VARIABLES
    public var goMovieDetail: MovieDetail?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadMovieDetail(poMovieDetail: goMovieDetail)
    }
    
    func loadMovieDetail(poMovieDetail: MovieDetail?) {
        if let movie = poMovieDetail {
            imgMovie.kf.setImage(with: URL(string: movie.img))
            lblMovieTitle.text = movie.title
            lblMovieDescription.text = movie.description
        }
    }
    
}
