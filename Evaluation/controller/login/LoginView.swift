//
//  LoginView.swift
//  Evaluation
//
//  Created by Paolo Eduardo Arámbulo Calderón on 9/9/20.
//  Copyright © 2020 Paolo Eduardo Arámbulo Calderón. All rights reserved.
//

import Foundation

protocol LoginView: DefaultView{
    
    func onSuccessLogin(psToken: String, psMail: String, psPassword: String)
    
}
