//
//  LoginRequest.swift
//  Evaluation
//
//  Created by Paolo Eduardo Arámbulo Calderón on 9/10/20.
//  Copyright © 2020 Paolo Eduardo Arámbulo Calderón. All rights reserved.
//

import Foundation

struct LoginRequest: BaseRequest {
    
    let email: String
    let password: String
    
    func toBody() -> NSDictionary {
        return ["email": email,
                "password": password]
    }
    
}
