//
//  MovieTableViewCell.swift
//  Evaluation
//
//  Created by Paolo Eduardo Arámbulo Calderón on 9/9/20.
//  Copyright © 2020 Paolo Eduardo Arámbulo Calderón. All rights reserved.
//

import UIKit
import Kingfisher

class MovieTableViewCell: UITableViewCell {

    @IBOutlet weak var imgMovie: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setData(poMovie: MovieDetail?) {
        if let loMovie = poMovie {
            lblTitle.text = loMovie.title
            imgMovie.kf.setImage(with: URL(string: loMovie.img))
        }
    }
    
}
