//
//  MoviePresenter.swift
//  Evaluation
//
//  Created by Paolo Eduardo Arámbulo Calderón on 9/10/20.
//  Copyright © 2020 Paolo Eduardo Arámbulo Calderón. All rights reserved.
//

import Foundation
import RxSwift

class MoviePresenter {
    
    private let goMovieService: MovieService
    private let goDisposeBag = DisposeBag()
    
    weak var goView: MovieView?
    
    init(poMovieService: MovieService) {
        self.goMovieService = poMovieService
    }

    func paginate(psToken: String) {
        
        goMovieService.paginate(psToken: psToken)
            .subscribe(onNext: { (poResponse) in
                if (poResponse.status == 200) {
                    self.goView?.onMovieList(paoMovie: poResponse.data!!)
                } else {
                    self.goView?.onError(psMessage: "An error ocurred")
                }
            }, onError: { (poError) in
                self.goView?.onError(psMessage: poError.localizedDescription)
            }).disposed(by: goDisposeBag)
        
    }
    
}
