//
//  RestBase.swift
//  Evaluation
//
//  Created by Paolo Eduardo Arámbulo Calderón on 9/10/20.
//  Copyright © 2020 Paolo Eduardo Arámbulo Calderón. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

class RestBase {
    
    enum HttpMethod: String {
        case POST = "POST"
        case GET = "GET"
    }
    
    static func request<T: Codable>(poRequest: BaseRequest?, psUrl: String, poMethod: HttpMethod, psToken: String? = nil) -> Observable<Response<T?>> {
        
        var loRequest = URLRequest(url: URL(string: psUrl)!)
        
        if let token = psToken {
            loRequest.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        }
        
        loRequest.httpMethod = poMethod.rawValue
        loRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            if let poRequest = poRequest {
                loRequest.httpBody = try JSONSerialization.data(withJSONObject: poRequest.toBody(), options: [])
            }
        } catch {
            print(error.localizedDescription)
        }
        
        return Observable.create { observer -> Disposable in
            
            let task = URLSession.shared.dataTask(with: loRequest) { (data, urlResponse, error) in
                
                guard let data = data else {
                    observer.onError(error ?? NSError(domain: "", code: -1, userInfo: nil))
                    print(error?.localizedDescription ?? "")
                    return
                }
                
                var loResponse: Response<T?>? = nil
                
                do{
                    loResponse = try JSONDecoder().decode(Response<T?>.self, from: data)
                    observer.onNext(loResponse!)
                }catch{
                    observer.onError(error)
                }
            }
            
            task.resume()
            
            return Disposables.create {
                task.cancel()
            }
        }
    }
}

