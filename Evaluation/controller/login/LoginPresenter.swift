//
//  LoginPresenter.swift
//  Evaluation
//
//  Created by Paolo Eduardo Arámbulo Calderón on 9/9/20.
//  Copyright © 2020 Paolo Eduardo Arámbulo Calderón. All rights reserved.
//

import Foundation
import RxSwift

class LoginPresenter {
    
    private let goAuthService: AuthService
    private let goDisposeBag = DisposeBag()
    
    weak var goView: LoginView?
    
    init(poAuthService: AuthService) {
        self.goAuthService = poAuthService
    }
    
    func login(psMail: String, psPassword: String) {
        
        let loLoginRequest = LoginRequest(email: psMail, password: psPassword)
        
        goAuthService.login(poLoginRequest: loLoginRequest)
            .subscribe(onNext: { (poResponse) in
                if (poResponse.status == 201) {
                    self.goView?.onSuccessLogin(psToken: poResponse.data!!.token, psMail: psMail, psPassword: psPassword)
                } else {
                    self.goView?.onError(psMessage: "An error ocurred")
                }
            }, onError: { (poError) in
                self.goView?.onError(psMessage: poError.localizedDescription)
            }).disposed(by: goDisposeBag)
        
    }
    
}
