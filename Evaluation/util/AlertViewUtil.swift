//
//  AlertViewUtil.swift
//  Evaluation
//
//  Created by Paolo Eduardo Arámbulo Calderón on 9/10/20.
//  Copyright © 2020 Paolo Eduardo Arámbulo Calderón. All rights reserved.
//

import Foundation
import UIKit


class AlertViewUtil {
    
    private init() {
        
    }
    
    public static func showAccept(poViewController: UIViewController, psTitle: String = "PERU APPS", psMessage: String, poOnAccept: AlertView.completion = nil) {
        AlertView(poViewController: poViewController).show(paoAction: .ACCEPT, psTitle: psTitle, psMessage: psMessage, onAccept: poOnAccept, onCancel: nil, onNone: nil)
    }
    
    public static func showAcceptAndCancel(poViewController: UIViewController, psTitle: String, psMessage: String, poOnAccept: AlertView.completion, poOnCancel: AlertView.completion) {
        AlertView(poViewController: poViewController).show(paoAction: .ACCEPT, .CANCEL, psMessage: psMessage, onAccept: poOnAccept, onCancel: poOnCancel, onNone: nil)
    }
}
