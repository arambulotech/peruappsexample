//
//  AuthService.swift
//  Evaluation
//
//  Created by Paolo Eduardo Arámbulo Calderón on 9/9/20.
//  Copyright © 2020 Paolo Eduardo Arámbulo Calderón. All rights reserved.
//

import Foundation
import RxSwift

class AuthService: AuthUseCase {
    
    func login(poLoginRequest: LoginRequest) -> Observable<Response<Token?>> {
        return RestBase.request(poRequest: poLoginRequest, psUrl: ApiConstants.security.login, poMethod: .POST)
    }
    
}
