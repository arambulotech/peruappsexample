//
//  BaseRouter.swift
//  Evaluation
//
//  Created by Paolo Eduardo Arámbulo Calderón on 9/10/20.
//  Copyright © 2020 Paolo Eduardo Arámbulo Calderón. All rights reserved.
//

import Foundation
import UIKit

class BaseRouter {
    weak var goViewController: UIViewController!
    
    init(poViewController: UIViewController) {
        self.goViewController = poViewController
    }

}
