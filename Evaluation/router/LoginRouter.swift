//
//  LoginRouter.swift
//  Evaluation
//
//  Created by Paolo Eduardo Arámbulo Calderón on 9/10/20.
//  Copyright © 2020 Paolo Eduardo Arámbulo Calderón. All rights reserved.
//

import Foundation
import UIKit

class LoginRouter: BaseRouter {
    func goToMain(psToken: String, psMail: String, psPassword: String){
//        let loStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let loViewController = loStoryboard.instantiateViewController(withIdentifier:
//            "idMovieViewController") as! MovieViewController
//        loViewController.gsToken = psToken
//        loViewController.gsMail = psMail
//        loViewController.gsPassword = psPassword
//        loViewController.modalPresentationStyle = .fullScreen
//        goViewController.present(loViewController, animated: true)
        goViewController.performSegue(withIdentifier: "toMain", sender: nil)
    }
}
