//
//  MovieUseCase.swift
//  Evaluation
//
//  Created by Paolo Eduardo Arámbulo Calderón on 9/10/20.
//  Copyright © 2020 Paolo Eduardo Arámbulo Calderón. All rights reserved.
//

import Foundation
import RxSwift

protocol MovieUseCase {
    
    func paginate(psToken: String) -> Observable<Response<[Movie]?>>
    
}
