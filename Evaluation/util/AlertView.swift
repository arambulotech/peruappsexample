//
//  AlertView.swift
//  Evaluation
//
//  Created by Paolo Eduardo Arámbulo Calderón on 9/10/20.
//  Copyright © 2020 Paolo Eduardo Arámbulo Calderón. All rights reserved.
//

import Foundation
import UIKit

class AlertView {
    enum Action {
        case ACCEPT, CANCEL, NONE
    }
    
    private let goViewController: UIViewController!
    
    typealias completion = ((UIAlertAction) -> Swift.Void)?
    
    init(poViewController: UIViewController) {
        self.goViewController = poViewController
    }
    
    func show(paoAction: AlertView.Action..., psTitle: String = "", psMessage: String, onAccept: AlertView.completion, onCancel: AlertView.completion, onNone: AlertView.completion) {
        let loAlertController = UIAlertController(title: psTitle, message: psMessage, preferredStyle: .alert)
        paoAction.forEach({
            let loAlertAction: UIAlertAction!
            switch $0 {
            case .ACCEPT:
                loAlertAction = UIAlertAction(title: AlertView.getTextButton(poAction: $0), style: .default, handler: onAccept)
            case .CANCEL:
                loAlertAction = UIAlertAction(title: AlertView.getTextButton(poAction: $0), style: .destructive, handler: onCancel)
            case .NONE:
                loAlertAction = UIAlertAction(title: AlertView.getTextButton(poAction: $0), style: .default, handler: onNone)
            }
            loAlertController.addAction(loAlertAction)
        })
        goViewController.present(loAlertController, animated: true, completion: nil)
    }
    
    private static func getTextButton(poAction: AlertView.Action) -> String {
        switch poAction {
        case .ACCEPT:
            return "Accept"
        case .CANCEL:
            return "Cancel"
        case .NONE:
            return "No"
        }
    }
}
