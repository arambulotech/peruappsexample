//
//  MovieViewController.swift
//  Evaluation
//
//  Created by Paolo Eduardo Arámbulo Calderón on 9/10/20.
//  Copyright © 2020 Paolo Eduardo Arámbulo Calderón. All rights reserved.
//

import UIKit
import Alamofire

class MovieViewController: UIViewController {
    
    //MARK: OUTLETS
    @IBOutlet weak var tbMovies: UITableView!
    
    //MARK: GLOBAL VARIABLES
    fileprivate var gaoMovies = [Movie]()
    private var goMovieRouter: MovieRouter!
    private var goMoviePresenter: MoviePresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initTableView()
        initRouter()
        initPresenter()
        
        if let token = UserDefaults.standard.string(forKey: "KEY_TOKEN") {
            goMoviePresenter.paginate(psToken: token)
        }
    }
    
    func initTableView() {
        tbMovies.delegate = self
        tbMovies.dataSource = self
        
        tbMovies.register(UINib(nibName: "MovieTableViewCell", bundle: nil), forCellReuseIdentifier: "idMovieTableViewCell")
    }
    
    func initRouter() {
        goMovieRouter = MovieRouter(poViewController: self)
    }
    
    func initPresenter() {
        goMoviePresenter = MoviePresenter(poMovieService: MovieService())
        goMoviePresenter.goView = self
    }
    
}

extension MovieViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gaoMovies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let loMovie = gaoMovies[indexPath.row]
        
        guard let loCell = tbMovies.dequeueReusableCell(withIdentifier: "idMovieTableViewCell") as? MovieTableViewCell else {
            return UITableViewCell()
        }
        
        loCell.setData(poMovie: loMovie.detail)
        return loCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let loMovie = gaoMovies[indexPath.row]
        
        goMovieRouter.goToDetail(poMovieDetail: loMovie.detail)
    }
}

extension MovieViewController: MovieView {
    
    func onMovieList(paoMovie: [Movie]) {
        gaoMovies = paoMovie
        
        DispatchQueue.main.async {
            self.tbMovies.reloadData()
        }
    }
    
    func onError(psMessage: String) {
        DispatchQueue.main.async {
            AlertViewUtil.showAccept(poViewController: self, psMessage: psMessage)
        }
    }
    
}
